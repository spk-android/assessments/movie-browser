![Project Logo](assets/logo.png)

## Description
Movie browsing Android application based on TMDb API, built with Kotlin, Retrofit and Glide

## Screenshots
[<img src="assets/scr1.jpg" width=160>](assets/scr1.jpg)
[<img src="assets/scr2.jpg" width=160>](assets/scr2.jpg)
[<img src="assets/scr3.jpg" width=160>](assets/scr3.jpg)
[<img src="assets/scr4.jpg" width=160>](assets/scr4.jpg)
[<img src="assets/scr5.jpg" width=160>](assets/scr5.jpg)

package com.spyrosk.movie_browser.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

fun <T> ViewModel.send(channel: Channel<T>, vararg events: T) {
	this.viewModelScope.launch {
		for(event in events)
			channel.send(event)
	}
}

package com.spyrosk.movie_browser.utils

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi

@SuppressLint("ObsoleteSdkInt")
fun connectionExists(connectivityManager: ConnectivityManager): Boolean {
	return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		checkForM(connectivityManager)
	else
		checkForOlderThanM(connectivityManager)
}

@RequiresApi(Build.VERSION_CODES.M)
private fun checkForM(connectivityManager: ConnectivityManager): Boolean {
	val activeNetwork = connectivityManager.activeNetwork ?: return false
	val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
	
	return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
			networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
			networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
}

@Suppress("DEPRECATION")
private fun checkForOlderThanM(connectivityManager: ConnectivityManager): Boolean {
	val activeNetwork = connectivityManager.activeNetworkInfo ?: return false
	
	return (activeNetwork.type == ConnectivityManager.TYPE_WIFI ||
			activeNetwork.type == ConnectivityManager.TYPE_MOBILE ||
			activeNetwork.type == ConnectivityManager.TYPE_ETHERNET)
}

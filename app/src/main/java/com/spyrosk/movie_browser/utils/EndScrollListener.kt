package com.spyrosk.movie_browser.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class EndScrollListener(private val callback: EndReachedCallback, private val threshold: Int = 0) : RecyclerView.OnScrollListener() {
	
	override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
		super.onScrolled(recyclerView, dx, dy)
		
		if(dy <= 0) return
		
		val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
		val lastItemPosition = (recyclerView.adapter as ListAdapter<*, *>).itemCount - 1
		
		if(lastItemPosition - lastVisibleItemPosition <= threshold)
			callback.onEndReached()
	}
	
	fun interface EndReachedCallback {
		fun onEndReached()
	}
	
}

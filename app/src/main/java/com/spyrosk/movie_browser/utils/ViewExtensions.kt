package com.spyrosk.movie_browser.utils

import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import com.bumptech.glide.Glide
import com.spyrosk.movie_browser.R
import com.spyrosk.movie_browser.api.ImageType
import com.spyrosk.movie_browser.api.ImageUrlCreator

inline fun SearchView.onQueryTextChanged(crossinline listener: (String) -> Unit) {
	this.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
		override fun onQueryTextSubmit(query: String?): Boolean = true		// indifferent
		
		override fun onQueryTextChange(newText: String?): Boolean {
			listener.invoke(newText.orEmpty())
			return true
		}
	})
}

fun ImageView.safeLoadWithGlide(parentView: View, imageUrlCreator: ImageUrlCreator, imageRelativePath: String?, imageType: ImageType) {
	this.post {
		val imageUrl = if(imageType == ImageType.POSTER)
			imageUrlCreator.getPosterUrl(imageRelativePath, this.width)
		else
			imageUrlCreator.getBackdropUrl(imageRelativePath, this.width)
		
		val fallbackId = R.drawable.ic_movie_image_fallback
		val placeholderId = R.drawable.ic_movie_image_fallback
		
		if(imageUrl.isNullOrBlank())
			Glide.with(parentView).load(fallbackId).placeholder(placeholderId).into(this)
		else
			Glide.with(parentView).load(imageUrl).placeholder(placeholderId).into(this)
	}
}

package com.spyrosk.movie_browser.data

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
	val id: String,
	val title: String,
	@Json(name = "release_date") val releaseDate: String?,
	@Json(name = "vote_average") val ratingOutOfTen: Float?,
	@Json(name = "poster_path") val posterRelativePath: String?,
	@Json(name = "backdrop_path") val backdropRelativePath: String?
) : Parcelable

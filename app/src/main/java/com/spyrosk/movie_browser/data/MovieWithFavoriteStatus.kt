package com.spyrosk.movie_browser.data

data class MovieWithFavoriteStatus (
	val movie: Movie,
	val isFavorite: Boolean
)

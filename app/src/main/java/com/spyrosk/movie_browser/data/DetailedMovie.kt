package com.spyrosk.movie_browser.data

import com.squareup.moshi.Json

data class DetailedMovie(
	val id: String,
	val title: String,
	@Json(name = "release_date") val releaseDate: String?,
	@Json(name = "vote_average") val ratingOutOfTen: Float?,
	@Json(name = "poster_path") val posterRelativePath: String?,
	@Json(name = "backdrop_path") val backdropRelativePath: String?,
	val genres: List<Genre>?,
	val overview: String,
	val credits: MovieCredits
) {
	val genresAsString: String
		get() = genres?.let {
			it.joinToString(", ") { g -> g.name }
		} ?: "unknown"
	
	val castAsString: String
		get() = credits.cast.joinToString(", ") { castMember -> castMember.name }
	
	val directorsAsString
		get() = credits.crew.filter { member -> member.job.lowercase() == "director" }
			.joinToString(", ") { d -> d.name }
	
	val backdropRelativePathWithFallback
		get() = backdropRelativePath ?: posterRelativePath
}

data class Genre(
	val id: String,
	val name: String
)

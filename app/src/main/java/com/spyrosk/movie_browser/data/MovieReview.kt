package com.spyrosk.movie_browser.data

import com.squareup.moshi.Json

data class MovieReview (
	val id: String,
	val author: String,
	val content: String,
	@Json(name = "created_at") val dateCreated: String,
	@Json(name = "updated_at") val dateUpdated: String
)

package com.spyrosk.movie_browser.data

data class MovieCredits(
	val cast: List<CastMember>,
	val crew: List<CrewMember>
)

data class CastMember(
	val id: String,
	val name: String,
	val character: String
)

data class CrewMember(
	val id: String,
	val name: String,
	val department: String,
	val job: String
)

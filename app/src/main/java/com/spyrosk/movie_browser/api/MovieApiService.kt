package com.spyrosk.movie_browser.api

import com.spyrosk.movie_browser.data.DetailedMovie
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.data.MovieReview
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiService {
	
	@GET("movie/popular")
	suspend fun getPopular(@Query("page") page: Int): PaginatedResult<Movie>
	
	@GET("search/movie")
	suspend fun getSearchResults(@Query("query") searchTerm: String, @Query("page") page: Int): PaginatedResult<Movie>
	
	@GET("movie/{movie_id}" + "?append_to_response=credits")
	suspend fun getMovieDetails(@Path("movie_id") movieId: String): DetailedMovie
	
	@GET("movie/{movie_id}/similar")
	suspend fun getSimilarMovies(@Path("movie_id") movieId: String): PaginatedResult<Movie>
	
	@GET("movie/{movie_id}/reviews")
	suspend fun getReviews(@Path("movie_id") movieId: String): PaginatedResult<MovieReview>
	
}

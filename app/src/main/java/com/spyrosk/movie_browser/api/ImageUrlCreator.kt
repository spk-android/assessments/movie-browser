package com.spyrosk.movie_browser.api

import com.spyrosk.movie_browser.AppConfig
import javax.inject.Inject

class ImageUrlCreator @Inject constructor(
	private val appConfig: AppConfig
) {
	
	private fun getImageUrl(relativePath: String?, size: String = "original"): String? {
		if(relativePath == null)
			return null
		
		return "${appConfig.imageBaseUrl}$size$relativePath"
	}
	
	fun getBackdropUrl(relativePath: String?, requiredWidth: Int = Int.MAX_VALUE): String? {
		for(s in BackdropSize.values()) {
			if(s.width >= requiredWidth)
				return getImageUrl(relativePath, s.toString().lowercase())
		}
		return null
	}
	
	fun getPosterUrl(relativePath: String?, requiredWidth: Int = Int.MAX_VALUE): String? {
		for(s in PosterSize.values()) {
			if(s.width >= requiredWidth)
				return getImageUrl(relativePath, s.toString().lowercase())
		}
		return null
	}
	
}

enum class ImageType {
	POSTER, BACKDROP
}

enum class BackdropSize(val width: Int) {
	W300(300),
	W780(780),
	W1280(1280),
	ORIGINAL(Int.MAX_VALUE)
}

enum class PosterSize(val width: Int) {
	W92(92),
	W154(154),
	W185(185),
	W342(342),
	W500(500),
	W780(780),
	ORIGINAL(Int.MAX_VALUE)
}

package com.spyrosk.movie_browser.ui.movie_list

import android.os.Bundle
import android.view.View
import android.widget.ToggleButton
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import androidx.recyclerview.widget.LinearLayoutManager
import com.spyrosk.movie_browser.R
import com.spyrosk.movie_browser.api.ImageUrlCreator
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.databinding.FragmentMovieListBinding
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import com.spyrosk.movie_browser.ui.movie_list.MovieListViewModel.MovieListEvent
import com.spyrosk.movie_browser.ui.movie_list.MovieListViewModel.MovieListEvent.*
import com.spyrosk.movie_browser.utils.EndScrollListener
import com.spyrosk.movie_browser.utils.onQueryTextChanged

@AndroidEntryPoint
class MovieListFragment : Fragment(R.layout.fragment_movie_list) {
	
	private lateinit var binding: FragmentMovieListBinding
	private val viewModel: MovieListViewModel by viewModels()
	
	@Inject
	lateinit var imageUrlCreator: ImageUrlCreator
	
	lateinit var searchView: SearchView
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		binding = FragmentMovieListBinding.bind(view)
		
		binding.rcvMovieList.layoutManager = LinearLayoutManager(requireContext())
		binding.rcvMovieList.adapter = MovieListAdapter(imageUrlCreator,
			onMovieClickListener = { m -> onMovieClicked(m) },
			onMovieFavoriteClickListener = { m, v -> onMovieFavoriteClicked(m, v) })
		binding.rcvMovieList.addOnScrollListener(EndScrollListener(viewModel::endOfListReached, 8))
		viewModel.moviesWithFavoriteStatusLiveData.observe(viewLifecycleOwner, (binding.rcvMovieList.adapter as MovieListAdapter)::submitList)
		
		viewModel.loadingMovies.observe(viewLifecycleOwner) { loading ->
			binding.progressbar.root.visibility = if(loading) View.VISIBLE else View.GONE
		}
		
		binding.swipeContainer.setOnRefreshListener(viewModel::refreshSwiped)
		
		binding.tbToolbar.inflateMenu(R.menu.menu_search)
		searchView = binding.tbToolbar.menu.findItem(R.id.act_search).actionView as SearchView
		searchView.queryHint = requireContext().getString(R.string.search_hint)
		searchView.onQueryTextChanged(viewModel::onSearchTextChanged)
		
		listenToViewModelEvents()
	}
	
	override fun onResume() {
		super.onResume()
		binding.rcvMovieList.requestFocus()
	}
	
	private fun onMovieClicked(movie: Movie) {
		viewModel.movieClicked(movie)
	}
	
	private fun onMovieFavoriteClicked(movie: Movie, buttonView: View) {
		if((buttonView as ToggleButton).isChecked)
			viewModel.favoriteChecked(movie)
		else
			viewModel.favoriteUnchecked(movie)
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.movieListEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: MovieListEvent) {
		when(event) {
			is NavigateToMovieDetails -> {
				binding.rcvMovieList.requestFocus()
				val action =
					MovieListFragmentDirections.actionMovieListFragmentToMovieDetailsFragment(event.movie)
				findNavController().navigate(action)
			}
			is RefreshComplete -> binding.swipeContainer.isRefreshing = false
			is ShowNoConnectionError -> Snackbar.make(requireView(), getString(R.string.no_connection_error_message), Snackbar.LENGTH_LONG).show()
			is ShowNetworkError -> Snackbar.make(requireView(), getString(R.string.generic_network_error_message), Snackbar.LENGTH_LONG).show()
			EndOfListReached -> Snackbar.make(requireView(), getString(R.string.end_of_movie_list_message), Snackbar.LENGTH_LONG).show()
		}
	}
	
}

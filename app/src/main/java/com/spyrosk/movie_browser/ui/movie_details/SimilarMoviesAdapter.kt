package com.spyrosk.movie_browser.ui.movie_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.spyrosk.movie_browser.api.ImageType
import com.spyrosk.movie_browser.api.ImageUrlCreator
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.databinding.ItemMovieCompactBinding
import com.spyrosk.movie_browser.utils.safeLoadWithGlide

class SimilarMoviesAdapter(
	private val imageUrlCreator: ImageUrlCreator,
	private val onMovieClickListener: OnMovieClickListener
) : RecyclerView.Adapter<SimilarMoviesAdapter.ViewHolder>() {
	
	var items = emptyList<Movie>()
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = ItemMovieCompactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}
	
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
	}
	
	override fun getItemCount(): Int {
		return items.size
	}
	
	fun interface OnMovieClickListener {
		fun onMovieClicked(movie: Movie)
	}
	
	inner class ViewHolder(private val itemMovieCompactBinding: ItemMovieCompactBinding) :
		RecyclerView.ViewHolder(itemMovieCompactBinding.root) {
		
		init {
			itemMovieCompactBinding.root.setOnClickListener {
				if(adapterPosition != RecyclerView.NO_POSITION)
					onMovieClickListener.onMovieClicked(items[adapterPosition])
			}
		}
		
		fun bind(movie: Movie) {
			itemMovieCompactBinding.imgMoviePoster.safeLoadWithGlide(itemMovieCompactBinding.root, imageUrlCreator, movie.posterRelativePath, ImageType.POSTER)
		}
		
	}
	
}

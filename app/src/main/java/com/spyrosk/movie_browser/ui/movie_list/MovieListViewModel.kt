package com.spyrosk.movie_browser.ui.movie_list

import android.net.ConnectivityManager
import android.util.Log
import androidx.lifecycle.*
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.api.MovieApiService
import com.spyrosk.movie_browser.data.MovieWithFavoriteStatus
import com.spyrosk.movie_browser.repository.PreferenceRepository
import com.spyrosk.movie_browser.utils.connectionExists
import com.spyrosk.movie_browser.utils.send
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

private const val TAG = "MovieListViewModel"

@HiltViewModel
class MovieListViewModel @Inject constructor(
	private val movieApiService: MovieApiService,
	private val connectivityManager: ConnectivityManager,
	private val preferenceRepository: PreferenceRepository
) : ViewModel() {
	
	private val movieListEventChannel = Channel<MovieListEvent>()
	val movieListEventFlow = movieListEventChannel.receiveAsFlow()
	
	private val moviesLiveData = MutableLiveData<List<Movie>>(emptyList())
	
	// Movie data (API) + favorite status data (DataStore)
	private val moviesWithFavoriteStatusFlow =
		combine(moviesLiveData.asFlow(), preferenceRepository.favoriteMoviesFlow) { movies, favoriteMovies ->
			movies.map { m -> MovieWithFavoriteStatus(m, favoriteMovies.contains(m.id)) }
		}
	val moviesWithFavoriteStatusLiveData = moviesWithFavoriteStatusFlow.asLiveData()
	
	private val _loadingMovies = MutableLiveData<Boolean>(false)
	val loadingMovies: LiveData<Boolean>
		get() = _loadingMovies
	
	private var targetPage = 0
	private var expectedPages: Int = Int.MAX_VALUE
	
	private val searchTermChannel = Channel<String>()
	private val searchTermFlow = searchTermChannel.receiveAsFlow()
	private var currentSearchTerm = ""
	
	init {
		resetTargetPage()
		getNextPage()
		
		observeSearchTerms()
	}
	
	private fun resetTargetPage() {
		targetPage = 1
	}
	
	private fun getNextPage() {
		if(!connectionExists(connectivityManager)) {
			send(movieListEventChannel, MovieListEvent.ShowNoConnectionError, MovieListEvent.RefreshComplete)
			_loadingMovies.value = false
			return
		}
		
		if(targetPage > expectedPages && targetPage > 1) {		// Avoid displaying the message 0 pages are available
			send(movieListEventChannel, MovieListEvent.EndOfListReached)
			_loadingMovies.value = false
			return
		}
		
		viewModelScope.launch {
			try {
				val results = if(currentSearchTerm.isNotBlank())
					movieApiService.getSearchResults(currentSearchTerm, targetPage)
				else
					movieApiService.getPopular(targetPage)
				
				moviesLiveData.postValue(moviesLiveData.value!! + results.results)
				expectedPages = results.totalPages
				targetPage++
			}
			catch(e: Exception) {
				Log.e(TAG, "Exception on loading movies")
				e.printStackTrace()
				movieListEventChannel.send(MovieListEvent.ShowNetworkError)
			}
			
			movieListEventChannel.send(MovieListEvent.RefreshComplete)
			_loadingMovies.postValue(false)
		}
	}
	
	private fun observeSearchTerms() {
		viewModelScope.launch {
			searchTermFlow.debounce(500L).distinctUntilChanged().collect {
				moviesLiveData.value = emptyList()
				resetTargetPage()
				currentSearchTerm = it
				getNextPage()
			}
		}
	}
	
	fun onSearchTextChanged(s: String) = send(searchTermChannel, s)
	
	fun movieClicked(movie: Movie) = send(movieListEventChannel, MovieListEvent.NavigateToMovieDetails(movie))
	
	fun favoriteChecked(movie: Movie) {
		viewModelScope.launch {
			preferenceRepository.addFavorite(movie.id)
		}
	}
	
	fun favoriteUnchecked(movie: Movie) {
		viewModelScope.launch {
			preferenceRepository.removeFavorite(movie.id)
		}
	}
	
	fun endOfListReached() {
		if(_loadingMovies.value!!)
			return
		
		_loadingMovies.value = true
		
		getNextPage()
	}
	
	fun refreshSwiped() {
		moviesLiveData.value = emptyList()
		resetTargetPage()
		getNextPage()
	}
	
	
	sealed class MovieListEvent {
		data class NavigateToMovieDetails(val movie: Movie) : MovieListEvent()
		object RefreshComplete : MovieListEvent()
		object ShowNoConnectionError : MovieListEvent()
		object ShowNetworkError : MovieListEvent()
		object EndOfListReached : MovieListEvent()
	}
	
}

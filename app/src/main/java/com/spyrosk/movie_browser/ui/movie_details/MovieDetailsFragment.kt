package com.spyrosk.movie_browser.ui.movie_details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.ToggleButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.spyrosk.movie_browser.R
import com.spyrosk.movie_browser.api.ImageType
import com.spyrosk.movie_browser.api.ImageUrlCreator
import com.spyrosk.movie_browser.data.DetailedMovie
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.data.MovieReview
import com.spyrosk.movie_browser.databinding.FragmentMovieDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import com.spyrosk.movie_browser.ui.movie_details.MovieDetailsViewModel.MovieDetailsEvent
import com.spyrosk.movie_browser.ui.movie_details.MovieDetailsViewModel.MovieDetailsEvent.*
import com.spyrosk.movie_browser.utils.safeLoadWithGlide

import kotlinx.coroutines.launch

@AndroidEntryPoint
class MovieDetailsFragment : Fragment(R.layout.fragment_movie_details) {
	
	private val viewModel: MovieDetailsViewModel by viewModels()
	private lateinit var binding: FragmentMovieDetailsBinding
	
	@Inject
	lateinit var imageUrlCreator: ImageUrlCreator
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		binding = FragmentMovieDetailsBinding.bind(view)
		
		updateBasicDetails(viewModel.movieFromArgs)
		viewModel.detailedMovieLiveData.observe(viewLifecycleOwner, this::updateSecondaryDetails)
		
		binding.fabBack.setOnClickListener { onBackClicked() }
		
		viewLifecycleOwner.lifecycleScope.launch { binding.btnFavorite.isChecked = viewModel.isMovieFavorite() }
		binding.btnFavorite.setOnClickListener(this::onFavoriteClicked)
		
		binding.rcvSimilarMovies.adapter = SimilarMoviesAdapter(imageUrlCreator, this::onMovieClicked)
		binding.rcvSimilarMovies.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		viewModel.similarMoviesLiveData.observe(viewLifecycleOwner, this::updateSimilarMovies)
		
		binding.rcvReviews.adapter = MovieReviewsAdapter()
		binding.rcvReviews.layoutManager = LinearLayoutManager(requireContext())
		viewModel.movieReviewsLiveData.observe(viewLifecycleOwner, this::updateReviews)
		
		listenToViewModelEvents()
	}
	
	private fun onBackClicked() {
		viewModel.backClicked()
	}
	
	private fun updateBasicDetails(movie: Movie) {
		with(binding) {
			if(movie.backdropRelativePath != null)
				imgBackdrop.safeLoadWithGlide(root, imageUrlCreator, movie.backdropRelativePath, ImageType.BACKDROP)
			else
				imgBackdrop.safeLoadWithGlide(root, imageUrlCreator, movie.posterRelativePath, ImageType.POSTER)
			
			tvTitle.text = movie.title
			movie.ratingOutOfTen?.let { rtbRating.rating = movie.ratingOutOfTen / 2 }
			updateDetail(tvDate, movie.releaseDate)
		}
	}
	
	private fun updateSecondaryDetails(detailedMovie: DetailedMovie?) {
		if(detailedMovie == null)
			return
		
		with(binding) {
			updateDetail(tvGenres, detailedMovie.genresAsString)
			updateDetail(tvDescriptionBody, detailedMovie.overview)
			updateDetail(tvDirectorBody, detailedMovie.directorsAsString)
			updateDetail(tvCastBody, detailedMovie.castAsString)
		}
	}
	
	private fun updateDetail(textView: TextView, text: String?) {
		if(text.isNullOrBlank())
			textView.text = getString(R.string.unavailable_info_message)
		else
			textView.text = text
	}
	
	@SuppressLint("NotifyDataSetChanged")
	private fun updateSimilarMovies(movies: List<Movie>) {
		binding.tvNoSimilarMoviesMessage.visibility = if(movies.isEmpty()) View.VISIBLE else View.GONE
		
		(binding.rcvSimilarMovies.adapter as SimilarMoviesAdapter).items = movies
		(binding.rcvSimilarMovies.adapter as SimilarMoviesAdapter).notifyDataSetChanged()
	}
	
	@SuppressLint("NotifyDataSetChanged")
	private fun updateReviews(reviews: List<MovieReview>) {
		binding.tvNoReviewsMessage.visibility = if(reviews.isEmpty()) View.VISIBLE else View.GONE
		
		(binding.rcvReviews.adapter as MovieReviewsAdapter).items = reviews
		(binding.rcvReviews.adapter as MovieReviewsAdapter).notifyDataSetChanged()
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.movieListEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: MovieDetailsEvent) {
		when(event) {
			is NavigateToMovieDetails -> {
				val action = MovieDetailsFragmentDirections.actionMovieDetailsFragmentSelf(event.movie)
				findNavController().navigate(action)
			}
			is NavigateBack -> {
				findNavController().popBackStack()
			}
			is ShowNoConnectionError -> Snackbar.make(requireView(), getString(R.string.no_connection_error_message), Snackbar.LENGTH_LONG).show()
			is ShowNetworkError -> Snackbar.make(requireView(), getString(R.string.generic_network_error_message), Snackbar.LENGTH_LONG).show()
		}
	}
	
	private fun onFavoriteClicked(buttonView: View) {
		if((buttonView as ToggleButton).isChecked)
			viewModel.favoriteChecked()
		else
			viewModel.favoriteUnchecked()
	}
	
	private fun onMovieClicked(movie: Movie) {
		viewModel.movieClicked(movie)
	}
	
}

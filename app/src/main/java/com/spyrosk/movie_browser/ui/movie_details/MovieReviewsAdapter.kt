package com.spyrosk.movie_browser.ui.movie_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.spyrosk.movie_browser.data.MovieReview
import com.spyrosk.movie_browser.databinding.ItemReviewBinding

class MovieReviewsAdapter : RecyclerView.Adapter<MovieReviewsAdapter.ViewHolder>() {
	
	var items = emptyList<MovieReview>()
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}
	
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
	}
	
	override fun getItemCount(): Int {
		return items.size
	}
	
	inner class ViewHolder(private val itemReviewBinding: ItemReviewBinding) :
		RecyclerView.ViewHolder(itemReviewBinding.root) {
		
		fun bind(review: MovieReview) {
			itemReviewBinding.tvAuthor.text = review.author
			itemReviewBinding.tvContent.text = review.content
		}
		
	}
	
}

package com.spyrosk.movie_browser.ui.movie_details

import android.net.ConnectivityManager
import android.util.Log
import androidx.lifecycle.*
import com.spyrosk.movie_browser.api.MovieApiService
import com.spyrosk.movie_browser.data.DetailedMovie
import com.spyrosk.movie_browser.data.Movie
import com.spyrosk.movie_browser.data.MovieReview
import com.spyrosk.movie_browser.repository.PreferenceRepository
import com.spyrosk.movie_browser.utils.connectionExists
import com.spyrosk.movie_browser.utils.send
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

private const val TAG = "MovieDetailsViewModel"

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
	private val movieApiService: MovieApiService,
	private val connectivityManager: ConnectivityManager,
	private val preferenceRepository: PreferenceRepository,
	savedState: SavedStateHandle
) : ViewModel() {
	
	private val movieListEventChannel = Channel<MovieDetailsEvent>()
	val movieListEventFlow = movieListEventChannel.receiveAsFlow()
	
	private val _movieFromArgs = savedState.get<Movie>("movie")!!
	val movieFromArgs
		get() = _movieFromArgs
	
	private val _detailedMovieLiveData = MutableLiveData<DetailedMovie>(null)
	val detailedMovieLiveData: LiveData<DetailedMovie>
		get() = _detailedMovieLiveData
	
	private val _similarMoviesLiveData = MutableLiveData<List<Movie>>(emptyList())
	val similarMoviesLiveData: LiveData<List<Movie>>
		get() = _similarMoviesLiveData
	
	private val _movieReviewsLiveData = MutableLiveData<List<MovieReview>>(emptyList())
	val movieReviewsLiveData: LiveData<List<MovieReview>>
		get() = _movieReviewsLiveData
	
	init {
		getMovieDetails()
		getSimilarMovies()
		getMovieReviews()
	}
	
	private fun getMovieDetails() {
		if(!connectionExists(connectivityManager)) {
			send(movieListEventChannel, MovieDetailsEvent.ShowNoConnectionError)
			return
		}
		
		viewModelScope.launch {
			try {
				val detailedMovie = movieApiService.getMovieDetails(movieId = movieFromArgs.id)
				_detailedMovieLiveData.postValue(detailedMovie)
			}
			catch(e: Exception) {
				Log.e(TAG, "Exception on loading movie details")
				e.printStackTrace()
				movieListEventChannel.send(MovieDetailsEvent.ShowNetworkError)
			}
		}
	}
	
	private fun getSimilarMovies() {
		if(!connectionExists(connectivityManager)) {
			send(movieListEventChannel, MovieDetailsEvent.ShowNoConnectionError)
			return
		}
		
		viewModelScope.launch {
			try {
				val similarMovies = movieApiService.getSimilarMovies(movieId = movieFromArgs.id)
				_similarMoviesLiveData.postValue(similarMovies.results)
			}
			catch(e: Exception) {
				Log.e(TAG, "Exception on loading similar movies")
				e.printStackTrace()
				movieListEventChannel.send(MovieDetailsEvent.ShowNetworkError)
			}
		}
	}
	
	private fun getMovieReviews() {
		if(!connectionExists(connectivityManager)) {
			send(movieListEventChannel, MovieDetailsEvent.ShowNoConnectionError)
			return
		}

		viewModelScope.launch {
			try {
				val movieReviews = movieApiService.getReviews(movieId = movieFromArgs.id)
				val limit = 2.coerceAtMost(movieReviews.results.size)
				_movieReviewsLiveData.postValue(movieReviews.results.subList(0, limit))
			}
			catch(e: Exception) {
				Log.e(TAG, "Exception on loading movie reviews")
				e.printStackTrace()
				movieListEventChannel.send(MovieDetailsEvent.ShowNetworkError)
			}
		}
	}
	
	fun movieClicked(movie: Movie) = send(movieListEventChannel, MovieDetailsEvent.NavigateToMovieDetails(movie))
	
	fun favoriteChecked() {
		viewModelScope.launch {
			preferenceRepository.addFavorite(movieFromArgs.id)
		}
	}
	
	fun favoriteUnchecked() {
		viewModelScope.launch {
			preferenceRepository.removeFavorite(movieFromArgs.id)
		}
	}
	
	suspend fun isMovieFavorite(): Boolean {
		return preferenceRepository.favoriteMoviesFlow.first().contains(movieFromArgs.id)
	}
	
	fun backClicked() = send(movieListEventChannel, MovieDetailsEvent.NavigateBack)
	
	
	sealed class MovieDetailsEvent {
		object NavigateBack : MovieDetailsEvent()
		data class NavigateToMovieDetails(val movie: Movie) : MovieDetailsEvent()
		object ShowNoConnectionError : MovieDetailsEvent()
		object ShowNetworkError : MovieDetailsEvent()
	}
	
}

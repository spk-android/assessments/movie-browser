package com.spyrosk.movie_browser

data class AppConfig(
	val apiKey: String,
	val apiBaseUrl: String,
	val imageBaseUrl: String
)
